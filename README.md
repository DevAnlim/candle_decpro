# Candle_DecPro

## Установка

1. Установите пакет `pkger` с помощью команды `go get`:
   ```bash
   go get github.com/markbates/pkger/cmd/pkger
2. Выполните команду `pkger`, чтобы упаковать ресурсы:
   ```bash
   pkger
3. Соберите проект:
   ```bash
   go build -o candle_decpro
## Использование
4. Запустите исполняемый файл:
   ```bash
   ./candle_decpro
