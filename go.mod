module github.com/DevAnlim/candle_decpro

go 1.20

require (
	github.com/adshao/go-binance/v2 v2.4.2
	github.com/markbates/pkger v0.17.1
	github.com/sirupsen/logrus v1.9.2
	go.etcd.io/bbolt v1.3.7
	gorm.io/driver/mysql v1.5.1
	gorm.io/gorm v1.25.1
)

require (
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/gobuffalo/here v0.6.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
