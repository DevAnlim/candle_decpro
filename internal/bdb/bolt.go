package bdb

import (
	"encoding/json"
	"fmt"

	"github.com/adshao/go-binance/v2/futures"
	bolt "go.etcd.io/bbolt"
)

type DB struct {
	*bolt.DB
}

func New(filename string) (*DB, error) {
	db, err := bolt.Open(filename, 0666, nil)
	if err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

func (DB *DB) Append(pair string, klines []*futures.Kline) error {
	return DB.Update(func(tx *bolt.Tx) error {
		bolt, err := tx.CreateBucketIfNotExists([]byte(pair))
		if err != nil {
			return err
		}

		for _, k := range klines {
			data, err := json.Marshal(k)
			if err != nil {
				return err
			}

			if err != nil {
				return err
			}

			err = bolt.
				Put([]byte(fmt.
					Sprintf("%d", k.OpenTime)), data)

			if err != nil {
				return err
			}
		}
		return nil
	})
}
