package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/DevAnlim/candle_decpro/model"
	"github.com/DevAnlim/candle_decpro/store"
	"github.com/iancoleman/orderedmap"
	"github.com/sirupsen/logrus"
)

var configFile string
var dbFile string
var pair string
var pairSymbol_s string
var pairSymbol_p string
var interval string
var startTime int64
var TimeSymbol int64

func main() {
	start := time.Now()

	// Код из первой программы
	//flag.StringVar(&dbFile, "db", "data.bdb", "database filename")
	flag.StringVar(&configFile, "config", "dbconfig.json", "config filename")
	flag.Parse()

	config := model.NewConfig()

	err := config.Load(configFile)
	if err != nil {
		panic(err)
	}

	Store, err := store.New(config)
	if err != nil {
		panic(err)
	}

	CandleTimeframe, err := config.GetCandleTimeframe()
	if err != nil {
		panic(err)
	}

	// Получаем данные из Store
	values, err := Store.GetFieldFutures([]string{"Symbol", "Pair", "TimeStart"})
	if err != nil {
		panic(err)
	}

	for _, value := range values {
		splitValues := strings.Split(value, ",")
		if len(splitValues) == 3 {
			pairSymbol_s = strings.ReplaceAll(splitValues[0]+splitValues[1], " ", "")
			pairSymbol_p = strings.ReplaceAll(splitValues[0], " ", "")
			dateTimeStr := splitValues[2]
			dateTimeStr = strings.ReplaceAll(dateTimeStr, "T", " ")
			dateTimeStr = strings.ReplaceAll(dateTimeStr, "+03:00", "")
			dateTimeStr = strings.TrimSpace(dateTimeStr)

			layout := "2006-01-02 15:04:05"
			location, err := time.LoadLocation("UTC")
			if err != nil {
				panic(err)
			}
			dateTime, err := time.ParseInLocation(layout, dateTimeStr, location)
			if err != nil {
				panic(err)
			}
			TimeSymbol = dateTime.UnixNano() / 1000

		}
	}
	interval = CandleTimeframe
	pair = pairSymbol_s

	startTime = 0
	defer Store.Close()

	if startTime == 0 {
		i := Store.GetFirst(pair)
		startTime = *i
		logrus.Info(startTime)
	}

	/*db, err := bdb.New(dbFile)
	if err != nil {
		panic(err)
	}
	defer db.Close()*/

	done := make(chan bool) // Канал для сигнала о завершении добавления данных

	go func() {
		client := Store.FuturesClient()
		logrus.Info(pair)

		for {
			klines, err := client.
				NewKlinesService().
				Symbol(pair).
				Interval(interval).
				StartTime(startTime).
				Limit(1500).
				Do(context.Background())

			if err != nil {
				panic(err)
			}

			//err = db.Append(pair, klines)

			if len(klines) == 0 {
				break
			}

			startTime = klines[len(klines)-1].OpenTime
			logrus.Infof("[%v] %v / %v / %v", time.Since(start).String(),
				startTime, time.Unix(startTime/1000, 0), len(klines))
			start = time.Now()
		}

		done <- true // Отправляем сигнал о завершении добавления данных в канал
	}()

	// Код из второй программы
	Futures, err := Store.GetFutures()
	if err != nil {
		panic(err)
	}

	host, err := config.GetCandleHost()
	if err != nil {
		panic(err)
	}

	hostai, err := config.GetAiHost()
	if err != nil {
		panic(err)
	}

	// Поднимаем HTTP-сервер на указанном хосте и порту
	go func() {
		log.Println("Запуск сервера на", host)
		log.Fatal(http.ListenAndServe(host, nil))
	}()

	// Горутина для отправки POST-запроса
	go func() {
		// Ожидаем сигнала о завершении добавления данных
		<-done

		// Ждем, пока сервер полностью запустится
		time.Sleep(time.Second)

		// Создаем упорядоченный мап данных для отправки
		data := orderedmap.New()
		data.Set("symbol", pairSymbol_p)
		data.Set("price_table", pairSymbol_p+"_price")
		data.Set("id", []int{1001, 1002})

		// Преобразуем данные в JSON
		jsonData, err := json.Marshal(data)
		if err != nil {
			log.Fatal("Ошибка преобразования данных в JSON:", err)
		}

		// Создаем новый запрос с данными JSON
		req, err := http.NewRequest("POST", hostai, bytes.NewBuffer(jsonData))
		if err != nil {
			log.Fatal("Ошибка создания запроса:", err)
		}

		// Устанавливаем заголовки
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Accept", "text/plain")

		// Отправляем запрос
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.Fatal("Ошибка отправки запроса:", err)
		}
		defer resp.Body.Close()

		log.Println("Запрос успешно отправлен")
	}()

	// Код из третьей программы
	err_d := Store.CreateTables(Futures)
	if err_d != nil {
		panic(err_d)
	}

	err = Store.FetchFutureSymbols(Futures)
	if err != nil {
		panic(err)
	}
}
