package model

import (
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	Host string `json:"db_host"`
	Port string `json:"db_port"`
	Name string `json:"db_name"`
	User string `json:"db_user"`
	Pass string `json:"db_pass"`

	CandleTimeframe string `json:"candle_timeframe"`
	CandleHost      string `json:"candle_host"`

	AiHost string `json:"ai_host"`

	Key    string `yaml:"exchange_apikey_master"`
	Secret string `yaml:"exchange_secretkey_master"`
}

func NewConfig() *Config {
	return &Config{}
}

func (c *Config) GetDSN() string {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		c.User, c.Pass, c.Host, c.Port, c.Name)
	return dsn
}

func (c *Config) Load(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	return json.NewDecoder(f).Decode(c)
}
