package model

import (
	_ "embed"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Price struct {
	ID        *int       `gorm:"column:id;NOT NULL"`
	Time      *time.Time `gorm:"column:time"`
	Open      string     `gorm:"column:open;NOT NULL"`
	High      string     `gorm:"column:high;NOT NULL"`
	Low       string     `gorm:"column:low;NOT NULL"`
	Close     string     `gorm:"column:close;NOT NULL"`
	point     int
	tableName string
}

var sqlCreatePriceTable = `CREATE TABLE ` + "`:tableName` (" +
	"`id` int(11) NOT NULL AUTO_INCREMENT," +
	"`time` datetime NOT NULL," +
	"`open` decimal(30,2) NOT NULL," +
	"`high` decimal(30,2) NOT NULL," +
	"`low` decimal(30,2) NOT NULL," +
	"`close` decimal(30,2) NOT NULL," +
	"`timestamp` int(11) NOT NULL," +
	"PRIMARY KEY (`id`)" +
	") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;"

var sqlCreatePriceIndex = `ALTER TABLE :tableName
  ADD PRIMARY KEY (id);
ALTER TABLE :tableName
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
COMMIT;`

func (Price *Price) SetPricePoint(i int) {
	Price.point = i
}

func (Price *Price) SetTableName(name string) {
	Price.tableName = name
}

func (Price *Price) TableName() string {
	return Price.tableName
}

func (Price *Price) CreateSQL() string {
	return query(sqlCreatePriceTable, map[string]string{
		"tableName": Price.tableName,
		"point":     strconv.Itoa(Price.point),
	})
}

func (Price *Price) CreateAutoIndexSQL() string {
	return query(sqlCreatePriceIndex, map[string]string{
		"tableName": Price.tableName,
	})
}

func str(s string) string { return fmt.Sprintf("'%s'", s) }
func query(source string, param map[string]string) string {
	for k, v := range param {
		source = strings.ReplaceAll(source, ":"+k, v)
	}
	return source
}
