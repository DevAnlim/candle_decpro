package model

import (
	"strings"
	"time"
)

type Symbol struct {
	Symbol              string  `gorm:"column:symbol;NOT NULL"`
	Pair                *string `gorm:"column:pair"`
	Market              string  `gorm:"column:market;NOT NULL"`
	TickStatus          int     `gorm:"column:tick_status;NOT NULL"`
	PriceTable          string  `gorm:"column:price_table;NOT NULL"`
	PriceDecimal        int     `gorm:"column:price_decimal;NOT NULL"`
	TimeStart           string  `gorm:"column:time_start;type:DATETIME"`
	TradingStatus       int     `gorm:"column:trading_status;NOT NULL"`
	Streams             string  `gorm:"column:streams;NOT NULL"`
	LeverageStep        string  `gorm:"column:leverage_step;NOT NULL"`
	SizeDecimal         int     `gorm:"column:size_decimal;NOT NULL"`
	CoinList            int     `gorm:"column:coin_list;NOT NULL"`
	ExchangeMarketLong  string  `gorm:"column:exchange_market_long;NOT NULL"`
	ExchangeMarketShort string  `gorm:"column:exchange_market_short;NOT NULL"`
	Fee                 string  `gorm:"column:fee;default:0.0800;NOT NULL"`
	PricePercent        string  `gorm:"column:price_percent;NOT NULL"`
}

func (s Symbol) TableName() string { return "1_symbols" }

func (s Symbol) GetTimeStart() *int64 {
	dateTimeStr := s.TimeStart
	dateTimeStr = strings.ReplaceAll(dateTimeStr, "T", " ")
	dateTimeStr = strings.ReplaceAll(dateTimeStr, "+03:00", "")
	dateTimeStr = strings.TrimSpace(dateTimeStr)

	layout := "2006-01-02 15:04:05"
	location, err := time.LoadLocation("UTC")
	if err != nil {
		panic(err)
	}
	dateTime, err := time.ParseInLocation(layout, dateTimeStr, location)
	if err != nil {
		panic(err)
	}
	ts := dateTime.UnixMilli()
	return &ts
}
