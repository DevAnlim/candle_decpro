package store

import (
	"github.com/adshao/go-binance/v2"
	"github.com/adshao/go-binance/v2/futures"
)

func (Store *Store) Futures() *futures.Client {
	return binance.NewFuturesClient(Store.config.Key, Store.config.Secret)
}
