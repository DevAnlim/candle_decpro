package store

import (
	"context"
	"strings"
	"time"
)

func (s *Store) finder(duration time.Duration, symbol string) func(d time.Time) time.Time {
	return func(d time.Time) time.Time {
		var i = 0
		for ; ; d = d.Add(-duration) {
			i++

			klines, err := s.Futures().
				NewKlinesService().
				Symbol(symbol).
				Interval("1h").
				StartTime(d.Add(-duration).UnixMilli()).
				EndTime(d.UnixMilli()).
				Limit(1).
				Do(context.Background())
			if err != nil {
				break
			}
			if len(klines) == 0 {
				break
			}
		}
		return d.Add(duration)
	}
}

func (s *Store) GetFirst(symbol string) *int64 {
	var year = s.finder(time.Hour*24*365, symbol)
	var month = s.finder(time.Hour*24*30, symbol)
	var day = s.finder(time.Hour*20, symbol)

	i := day(
		month(
			year(time.Now()))).
		UnixMilli()

	return &i
}

func (s *Store) GetPoints(symbol string) int {
	i := s.GetFirst(symbol)

	klines, err := s.Futures().
		NewKlinesService().
		Symbol(symbol).
		Interval("1h").
		StartTime(*i).
		Limit(1).
		Do(context.Background())
	if err != nil {
		return 1
	}
	k := klines[0]
	ar := strings.Split(k.High, ".")

	return len(ar[1])
}
