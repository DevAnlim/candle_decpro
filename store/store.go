package store

import (
	"context"
	_ "embed"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/DevAnlim/candle_decpro/model"
	"github.com/adshao/go-binance/v2"
	"github.com/adshao/go-binance/v2/futures"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

const createdFormat = "2006-01-02 15:04:05"

type Store struct {
	db     *gorm.DB
	config *model.Config
}

func New(config *model.Config) (*Store, error) {
	db, err := gorm.Open(mysql.Open(config.GetDSN()), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	return &Store{db, config}, nil
}
func (s *Store) Close() {
	conn, err := s.db.DB()
	if err != nil {
		return
	}
	conn.Close()
}

func (s Store) GetFutures() ([]model.Symbol, error) {
	symbols := []model.Symbol{}
	res := s.db.
		Where("tick_status=? ", 1).
		Where("market=?", "futures").
		Find(&symbols)
	return symbols, res.Error
}

func (s Store) GetFieldFutures(fields []string) ([]string, error) {
	// Установите уровень логирования на Warn или Error, чтобы скрыть вывод медленных SQL-запросов
	s.db.Logger.LogMode(logger.Warn)
	symbols := []model.Symbol{}
	res := s.db.
		Where("tick_status=?", 1).
		Where("market=?", "futures").
		Find(&symbols)

	values := make([]string, len(symbols))
	for i, symbol := range symbols {
		for _, field := range fields {
			value := reflect.ValueOf(symbol)
			fieldValue := value.FieldByName(field)
			if fieldValue.Kind() == reflect.Ptr && !fieldValue.IsNil() {
				values[i] += fieldValue.Elem().String() + ", "
			} else {
				values[i] += fieldValue.String() + ", "
			}
		}
		values[i] = strings.TrimSuffix(values[i], ", ")
	}
	// Восстанавливаем режим логгирования по умолчанию
	//s.db.Logger.LogMode(logger.Info)
	return values, res.Error
}

func (s Store) Exec(sql string) *gorm.DB {
	return s.db.Exec(sql)
}

func (s Store) Last(price *model.Price) *gorm.DB {
	return s.db.Last(&price)
}

func (s Store) CreateTables(Futures []model.Symbol) error {
	// Установите уровень логирования на Warn или Error, чтобы скрыть вывод медленных SQL-запросов
	s.db.Logger.LogMode(logger.Warn)

	for _, k := range Futures {
		// Создаем таблицу
		tableName := fmt.Sprintf("%s_price", k.Symbol)

		// Проверяем наличие таблицы
		tableExists := s.db.Migrator().HasTable(tableName)
		if tableExists {
			continue // Пропускаем создание таблицы, если она уже существует
		}

		createTableQuery := "CREATE TABLE `" + tableName + "` (" +
			"`id` int(11) NOT NULL AUTO_INCREMENT," +
			"`time` datetime NOT NULL," +
			"`open` decimal(30,2) NOT NULL," +
			"`high` decimal(30,2) NOT NULL," +
			"`low` decimal(30,2) NOT NULL," +
			"`close` decimal(30,2) NOT NULL," +
			"`timestamp` int(11) NOT NULL," +
			"PRIMARY KEY (`id`)" +
			") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;"

		// Создаем таблицу
		err := s.db.Exec(createTableQuery).Error
		if err != nil {
			return err
		}

		// Задержка в 3 секунды
		time.Sleep(3 * time.Second)

		// Выполняем ALTER TABLE
		alterTableQuery := "ALTER TABLE `" + tableName + "`" +
			" ADD UNIQUE KEY `time` (`time`);"
		err = s.db.Exec(alterTableQuery).Error
		if err != nil {
			return err
		}

		alterTableQuery = "ALTER TABLE `" + tableName + "`" +
			" MODIFY id int(11) AUTO_INCREMENT;"
		err = s.db.Exec(alterTableQuery).Error
		if err != nil {
			return err
		}
	}

	// Восстанавливаем режим логгирования по умолчанию
	//s.db.Logger.LogMode(logger.Info)
	return nil
}

func (Store *Store) FetchFutureSymbols(symbols []model.Symbol) error {
	for _, k := range symbols {
		err := Store.FetchSymbol(k)
		if err != nil {
			return err
		}
	}
	return nil
}

func (Store *Store) FetchSymbol(Symbol model.Symbol) error {
	if Symbol.Pair == nil {
		return fmt.Errorf("symbol pair not set")
	}
	pair := Symbol.Symbol + *Symbol.Pair

	startTime := Symbol.GetTimeStart()
	if startTime == nil {
		startTime = Store.GetFirst(pair)
	}
	if startTime == nil {
		return fmt.Errorf("start time")
	}

	client := binance.NewFuturesClient(Store.config.Key, Store.config.Secret)

	klines, err := client.
		NewKlinesService().
		Symbol(pair).
		Interval(Store.config.CandleTimeframe).
		StartTime(*startTime).
		Limit(1500).
		Do(context.Background())
	if err != nil {
		return err
	}
	// Установите уровень логирования на Warn или Error, чтобы скрыть вывод медленных SQL-запросов
	Store.db.Logger.LogMode(logger.Warn)
	for _, k := range klines {
		newDate := dateFormat(k.OpenTime)
		unixTime, err := convertToUnixTime(removeStamp(newDate))
		if err != nil {
			panic(err)
		}
		myTime := strconv.Itoa(int(unixTime))

		var sqlPriceSelect = "SELECT COUNT(*) FROM `" + Symbol.Symbol + "_price" + "` WHERE `timestamp` = ?;"

		var count int
		err = Store.db.Raw(sqlPriceSelect, myTime).Scan(&count).Error
		if err != nil {
			return err
		}

		if count == 0 {
			var sqlPriceInsert = "INSERT INTO `" + Symbol.Symbol + "_price" + "` (`time`, `open`, `high`, `low`, `close`, `timestamp`) " +
				"VALUES (?, ?, ?, ?, ?, ?);"

			err = Store.db.Exec(sqlPriceInsert,
				newDate,
				k.Open,
				k.High,
				k.Low,
				k.Close,
				myTime,
			).Error
			if err != nil {
				return err
			}
		}
	}
	// Восстанавливаем режим логгирования по умолчанию
	// Store.db.Logger.LogMode(logger.Info)
	return nil
}

func removeStamp(str string) string {
	if strings.HasSuffix(str, "stamp") {
		str = strings.TrimSuffix(str, "stamp")
	}
	return str
}

func convertToUnixTime(datetimeStr string) (int64, error) {
	layout := "2006-01-02 15:04:05"
	t, err := time.Parse(layout, datetimeStr)
	if err != nil {
		return 0, err
	}

	unixTime := t.Unix()
	return unixTime, nil
}

func (s *Store) FuturesClient() *futures.Client {
	return binance.NewFuturesClient(s.config.Key, s.config.Secret)
}

func dateFormat(i int64) string {
	_, offset := time.Now().Zone()
	return time.Unix(i/1000, 0).
		Add(-time.Duration(offset) * time.Second).
		Format(createdFormat)
}

func str(s string) string { return fmt.Sprintf("'%s'", s) }

func query(source string, param map[string]string) string {
	for k, v := range param {
		source = strings.ReplaceAll(source, ":"+k, v)
	}
	return source
}
