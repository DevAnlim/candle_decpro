package store

import "time"

var TimeFrame = map[string]int64{
	"1s":  time.Second.Milliseconds(),
	"1m":  time.Minute.Milliseconds(),
	"3m":  3 * time.Minute.Milliseconds(),
	"5m":  5 * time.Minute.Milliseconds(),
	"15m": 15 * time.Minute.Milliseconds(),
	"30m": 30 * time.Minute.Milliseconds(),
	"1h":  time.Hour.Milliseconds(),
	"2h":  2 * time.Hour.Milliseconds(),
	"4h":  4 * time.Hour.Milliseconds(),
	"6h":  6 * time.Hour.Milliseconds(),
	"8h":  8 * time.Hour.Milliseconds(),
	"12h": 12 * time.Hour.Milliseconds(),
	"1d":  24 * time.Hour.Milliseconds(),
	"3d":  3 * 24 * time.Hour.Milliseconds(),
	"1w":  7 * 24 * time.Hour.Milliseconds(),
	"1M":  30 * 24 * time.Hour.Milliseconds(),
}
